﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Mvc;
using AutoMapper;
using Litium.Runtime.AutoMapper;
using JetBrains.Annotations;
using Litium.Accelerator.Builders;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Extensions;
using Litium.FieldFramework;
using Litium.FieldFramework.FieldTypes;
using Litium.Web.Models;
using Litium.Web.Models.Blocks;
namespace Litium.Accelerator.ViewModels.Block
{
    public class ApplicationFormBlockItemViewModel : IViewModel, IAutoMapperConfiguration
    {
        public string LabelText { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public bool IsMandatory { get; set; }
        public string FieldType { get; set; }
        public string FieldValue { get; set; }
        public string Input { get; set; }
        public List<SelectListItem> Options { get; set; }
        [UsedImplicitly]
        void IAutoMapperConfiguration.Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<MultiFieldItem, ApplicationFormBlockItemViewModel>()
             .ForMember(x => x.LabelText, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.LabelText, CultureInfo.CurrentUICulture)))
             .ForMember(x => x.Description, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.Description, CultureInfo.CurrentUICulture)))
             .ForMember(x => x.IsMandatory, m => m.MapFrom(p => p.Fields.GetValue<bool>(BlockFieldNameConstants.IsMandatory)))
             .ForMember(x => x.FieldType, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.FieldType)))
             .ForMember(x => x.FieldValue, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.FieldValue, CultureInfo.CurrentUICulture)));
        }
    }
}