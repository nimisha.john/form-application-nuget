﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Litium.Runtime.AutoMapper;
using JetBrains.Annotations;
using Litium.Accelerator.Builders;
using Litium.Accelerator.Constants;
using Litium.Accelerator.Extensions;
using Litium.FieldFramework;
using Litium.FieldFramework.FieldTypes;
using Litium.Web.Models;
using Litium.Web.Models.Blocks;
namespace Litium.Accelerator.ViewModels.Block
{
    public class ApplicationFormBlockViewModel : IViewModel, IAutoMapperConfiguration
    {
        public string Title { get; set; }
        public string EmailFrom { get; set; }
        public string EmailTo { get; set; }
        public string GdprLink { get; set; }
        public string ButtonText { get; set; }
        public string GdprLinkText { get; set; }
        [AllowHtml]
        public string TermsAndConditions { get; set; }
        public string TermsAndConditionsLabel { get; set; }
        public bool AgreeTermsAndConditions { get; set; }
        public List<ApplicationFormBlockItemViewModel> Fields { get; set; }
        [UsedImplicitly]
        void IAutoMapperConfiguration.Configure(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<BlockModel, ApplicationFormBlockViewModel>()
             .ForMember(x => x.Title, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.BlockTitle, CultureInfo.CurrentUICulture)))
             .ForMember(x => x.EmailFrom, m => m.MapFromField(BlockFieldNameConstants.EmailFrom))
             .ForMember(x => x.EmailTo, m => m.MapFromField(BlockFieldNameConstants.EmailTo))
             .ForMember(x => x.GdprLink, m => m.MapFrom(p => p.Fields.GetValue<PointerPageItem>(BlockFieldNameConstants.GDPRLink).MapTo<LinkModel>().Href))
             .ForMember(x => x.ButtonText, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.ButtonText, CultureInfo.CurrentUICulture)))
             .ForMember(x => x.Fields, m => m.MapFrom<FieldResolver>())
             .ForMember(x => x.GdprLinkText, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.GDPRLinkText, CultureInfo.CurrentUICulture)))
             .ForMember(x => x.TermsAndConditions, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.TermsAndConditions, CultureInfo.CurrentUICulture)))
             .ForMember(x => x.TermsAndConditionsLabel, m => m.MapFrom(p => p.Fields.GetValue<string>(BlockFieldNameConstants.TermsAndConditionsLabel, CultureInfo.CurrentUICulture)));
        }
        private class FieldResolver : IValueResolver<BlockModel, ApplicationFormBlockViewModel, List<ApplicationFormBlockItemViewModel>>
        {
            public List<ApplicationFormBlockItemViewModel> Resolve(BlockModel blockModel, ApplicationFormBlockViewModel source, List<ApplicationFormBlockItemViewModel> destMember, ResolutionContext context)
            {
                var result = new List<ApplicationFormBlockItemViewModel>();
                var fields = blockModel.Fields.GetValue<IList<MultiFieldItem>>(BlockFieldNameConstants.Fields);
                if (fields != null && fields.Count > 0)
                {
                    result.AddRange(fields.Select(x => x.MapTo<ApplicationFormBlockItemViewModel>()).ToList());
                }
                return result;
            }
        }
    }
}