﻿using Litium.Accelerator.ViewModels.Block;
using Litium.Runtime.AutoMapper;
using Litium.Web.Models.Blocks;
namespace Litium.Accelerator.Builders.Block
{
    public class ApplicationFormBlockViewModelBuilder : IViewModelBuilder<ApplicationFormBlockViewModel>
    {
        /// <summary>
        /// Build the new block view model
        /// </summary>
        /// <param name="blockModel">The current block</param>
        /// <returns>Return the new block view model</returns>
        public virtual ApplicationFormBlockViewModel Build(BlockModel blockModel)
        {
            return blockModel.MapTo<ApplicationFormBlockViewModel>();
        }
    }
}
