﻿using System;
using Litium.Accelerator.Constants;
using Litium.Blocks;
using Litium.FieldFramework;
using System.Collections.Generic;

namespace Litium.Accelerator.Definitions.Blocks
{
    internal class ApplicationFormBlockTemplateSetup : FieldTemplateSetup
    {
        private readonly CategoryService _categoryService;

        public ApplicationFormBlockTemplateSetup(CategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public override IEnumerable<FieldTemplate> GetTemplates()
        {
            var pageCategoryId = _categoryService.Get(BlockCategoryNameConstants.Pages)?.SystemId ?? Guid.Empty;
            var templates = new List<FieldTemplate>
            {
                new BlockFieldTemplate(BlockTemplateNameConstants.ApplicationForm)
                {
                    CategorySystemId = pageCategoryId,
                    Icon = "fas fa-bullhorn",
                    FieldGroups = new[]
                    {
                        new FieldTemplateFieldGroup()
                        {
                            Id = "General",
                            Collapsed = false,
                            Fields =
                            {
                                SystemFieldDefinitionConstants.Name,
                            }
                        },
                        new FieldTemplateFieldGroup()
                        {
                            Id = "Content",
                            Collapsed = false,
                            Fields =
                            {
                                BlockFieldNameConstants.BlockTitle,
                                BlockFieldNameConstants.EmailFrom,
                                BlockFieldNameConstants.EmailTo,
                                BlockFieldNameConstants.GDPRLinkText,
                                BlockFieldNameConstants.GDPRLink,
                                BlockFieldNameConstants.ButtonText,
                                BlockFieldNameConstants.TermsAndConditionsLabel,
                                BlockFieldNameConstants.TermsAndConditions,
                                BlockFieldNameConstants.Fields
                            }
                        },
                    }
                },
            };
            return templates;
        }
    }
}