﻿.application-form {
    .m-b-20 {
        margin-bottom: 20px;
    }

    input[type="text"], input[type="number"], input[type="email"] {
        width: 100%;
        height: 35px;
    }

    .radio-label {
        margin-right: 10px;
    }

    select {
        height: 35px;
    }

    textarea {
        width: 100%;
    }

    .alert {
        position: fixed;
        top: 70px;
        border: solid 1px #f5c6cb;
        width: auto;
        z-index: 999;
        left: 50%;
        border-radius: 4px;
        color: #721c24;
        padding: 8px 12px;
        font-size: 14px;
        background-color: #f8d7da;
        transform: translateX(-50%);
    }

    .success {
        position: fixed;
        top: 70px;
        border: solid 1px #c3e6cb;
        width: auto;
        z-index: 999;
        left: 50%;
        border-radius: 4px;
        padding: 8px 12px;
        font-size: 14px;
        transform: translateX(-50%);
        color: #155724;
        background-color: #d4edda;
    }
}
