﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using Litium.Accelerator.Builders.Block;
using Litium.Accelerator.Services;
using Litium.Accelerator.ViewModels.Block;
using Litium.Studio.Extenssions;
using Litium.Studio.Plugins.EmailValidation;
using Litium.Web.Models.Blocks;
using Litium.Web.Models.Websites;
namespace Litium.Accelerator.Mvc.Controllers.Blocks
{
    public class ApplicationFormBlockController : ControllerBase
    {
        private readonly ApplicationFormBlockViewModelBuilder _builder;
        private readonly IEmailValidator _emailValidator;
        private readonly MailService _mailService;
        public ApplicationFormBlockController(ApplicationFormBlockViewModelBuilder builder, IEmailValidator emailValidator, MailService mailService)
        {
            _builder = builder;
            _emailValidator = emailValidator;
            _mailService = mailService;
        }
        [HttpGet]
        public ActionResult Index(BlockModel currentBlockModel)
        {
            ApplicationFormBlockViewModel model;
            if (Session[ApplicationFormContstants.AfterPost] != null)
            {
                model = (ApplicationFormBlockViewModel)Session["AfterPost"];
                Session[ApplicationFormContstants.AfterPost] = null;
                ProcessDataAfterPost(model);
                return PartialView("~/Views/Block/ApplicationForm.cshtml", model);
            }
            model = _builder.Build(currentBlockModel);
            model?.Fields?.ForEach(x =>
            {
                switch (x.FieldType)
                {
                    case ApplicationFormContstants.CheckBox:
                    case ApplicationFormContstants.RadioButton:
                    case ApplicationFormContstants.Dropdown:
                        x.Options = new List<SelectListItem>();
                        var options = x.FieldValue?.Split(',');
                        if (options?.Length > 0)
                        {
                            x.Options.AddRange(options.Select(v => new SelectListItem { Text = v, Value = v }));
                        }
                        break;
                }
            });
            return PartialView("~/Views/Block/ApplicationForm.cshtml", model);
        }
        [HttpPost]
        public ActionResult Index(PageModel blockModel, ApplicationFormBlockViewModel model)
        {
            Session[ApplicationFormContstants.AfterPost] = model;
            return new RedirectResult(Request?.UrlReferrer?.ToString() ?? "~/Views/Block/ApplicationForm.cshtml");
        }
        public void ProcessDataAfterPost(ApplicationFormBlockViewModel model)
        {
            StringBuilder sb = new StringBuilder();
            Validate(model);
            if (ModelState.IsValid)
            {
                model.Fields.ForEach(x =>
                {
                    if (x.FieldType != ApplicationFormContstants.FileUpload)
                    {
                        sb.AppendLine($"{x.LabelText} : {GetData(x)}");
                    }
                });
                SendEmail(model.EmailFrom, model.EmailTo, model.Title, sb.ToString(), GetAttachments());
                ViewBag.SuccessMsg = "Success";
            }
        }
        private void Validate(ApplicationFormBlockViewModel model)
        {
            for (int i = 0; i < model.Fields.Count; i++)
            {
                var field = model.Fields[i];
                var hasInputData = !string.IsNullOrWhiteSpace(GetData(field, i));
                if (field.IsMandatory && !hasInputData)
                {
                    ModelState.AddModelError($"{field.LabelText}-{i}-Mandatory", $"{field.LabelText}");
                    continue;
                }
                if (hasInputData && !IsValid(field, i))
                {
                    ModelState.AddModelError($"{field.LabelText}-{i}-Invalid", $@"{field.LabelText} {(field.FieldType == ApplicationFormContstants.FileUpload ? "- file size upto 1 MB" : "")}");
                }
            }
            if (!string.IsNullOrWhiteSpace(model.TermsAndConditions) && !string.IsNullOrWhiteSpace(model.TermsAndConditionsLabel) && !model.AgreeTermsAndConditions)
            {
                ModelState.AddModelError($"AgreeTermsAndConditions-TermsAndConditions", "applicationform.agreetermsandconditions".AsWebSiteString());
            }
        }
        private void SendEmail(string from, string to, string subject, string body, List<HttpPostedFileBase> attachments)
        {
            if (_emailValidator.IsValidEmail(to) && _emailValidator.IsValidEmail(from))
            {
                _mailService.SendMailWithAttachment(from, to, subject, body, SmtpDeliveryMethod.PickupDirectoryFromIis, attachments);
            }
        }
        private bool IsValid(ApplicationFormBlockItemViewModel field, int index)
        {
            bool isValid;
            switch (field.FieldType)
            {
                case ApplicationFormContstants.Integer:
                    isValid = Regex.IsMatch(field.Input, @"^\d+$");
                    break;
                case ApplicationFormContstants.Email:
                    isValid = _emailValidator.IsValidEmail(field.Input);
                    break;
                case ApplicationFormContstants.FileUpload:
                    var file = Request.Files[$"fileUpload[{index}]"];
                    isValid = file != null && file.ContentLength < 1048576;//1 MB
                    break;
                default:
                    isValid = true;
                    break;
            }
            return isValid;
        }
        private string GetData(ApplicationFormBlockItemViewModel field, int index = 0)
        {
            var data = string.Empty;
            switch (field.FieldType)
            {
                case ApplicationFormContstants.Text:
                case ApplicationFormContstants.MultirowText:
                case ApplicationFormContstants.Integer:
                case ApplicationFormContstants.Email:
                case ApplicationFormContstants.Dropdown:
                case ApplicationFormContstants.RadioButton:
                    data = field.Input;
                    break;
                case ApplicationFormContstants.FileUpload:
                    var file = Request.Files[$"fileUpload[{index}]"];// Request.Files[0];
                    data = file != null && file.ContentLength > 0 ? file.FileName : "";
                    break;
                case ApplicationFormContstants.CheckBox:
                    var selectedItems = field.Options.Where(x => x.Selected).Select(x => x.Text);
                    data = String.Join(",", selectedItems);
                    break;
            }
            return data;
        }
        private List<HttpPostedFileBase> GetAttachments()
        {
            var attachments = new List<HttpPostedFileBase>();
            if (Request.Files.Count > 0)
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    if (file != null && file.ContentLength > 0)
                    {
                        attachments.Add(file);
                    }
                }
            }
            return attachments;
        }
    }
    public static class ApplicationFormContstants
    {
        public const string Text = "Text";
        public const string MultirowText = "MultirowText";
        public const string Integer = "Integer";
        public const string FileUpload = "FileUpload";
        public const string Dropdown = "Dropdown";
        public const string CheckBox = "CheckBox";
        public const string RadioButton = "RadioButton";
        public const string Email = "Email";
        public const string AfterPost = "AfterPost";
    }
}
