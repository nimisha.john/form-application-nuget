﻿@model Litium.Accelerator.ViewModels.Block.ApplicationFormBlockViewModel
@using (Html.BeginForm("Index", "ApplicationFormBlock", FormMethod.Post, new { enctype = "multipart/form-data" }))
{
    <div class="application-form">
        <div class="row m-b-20">
            <div class="small-12 medium-8 columns">
                <h1>@Model.Title</h1>
            </div>
        </div>
        @for (int i = 0; i < Model.Fields.Count; i++)
        {
            var field = Model.Fields[i];
            <div class="row m-b-20">
                <div class="small-1 columns">
                    @Model.Fields[i].LabelText :
                </div>
                <div class="small-4 columns">
                    @switch (field.FieldType)
                    {
                        case "Text":
                        case "Integer":
                        case "Email":
                            <input name="@($"Fields[{i}].Input")" id="@($"Fields_{i}__Input")" type="@(field.FieldType=="Integer" ? "number":field.FieldType.ToLower())" @(field.IsMandatory ? " required" : "") value="@(field.Input)" />
                            break;
                        case "MultirowText":
                            @Html.TextAreaFor(x => x.Fields[i].Input, new { required = field.IsMandatory ? "required" : "" })
                            break;
                        case "FileUpload":
                            <input type="file" name="@($"fileUpload[{i}]")" id="@($"fileUpload_{i}")" required="@(field.IsMandatory?"required":"")" />
                            break;
                        case "Dropdown":
                            @Html.DropDownListFor(x => x.Fields[i].Input, Model.Fields[i].Options, "-- Select value --")
                            ;
                            for (int index = 0; index < Model.Fields[i].Options.Count; index++)
                            {
                                @Html.HiddenFor(x => Model.Fields[i].Options[index].Text)
                                @Html.HiddenFor(x => Model.Fields[i].Options[index].Value)
                            }
                            break;
                        case "CheckBox":
                            for (int index = 0; index < Model.Fields[i].Options.Count; index++)
                            {
                                @Html.CheckBoxFor(x => Model.Fields[i].Options[index].Selected)
                                <label class="radio-label"> @Model.Fields[i].Options[index].Text</label>
                                @Html.HiddenFor(x => Model.Fields[i].Options[index].Text)
                                @Html.HiddenFor(x => Model.Fields[i].Options[index].Value)
                            }
                            break;
                        case "RadioButton":
                            for (int index = 0; index < Model.Fields[i].Options.Count; index++)
                            {
                                @Html.RadioButtonFor(x => x.Fields[i].Input, Model.Fields[i].Options[index].Text)
                                <label class="radio-label">@Model.Fields[i].Options[index].Text</label>
                                @Html.HiddenFor(x => Model.Fields[i].Options[index].Text)
                                @Html.HiddenFor(x => Model.Fields[i].Options[index].Value)
                            }
                            break;
                    }
                    @Html.HiddenFor(x => x.Fields[i].LabelText)
                    @Html.HiddenFor(x => Model.Fields[i].Description)
                    @Html.HiddenFor(x => x.Fields[i].FieldType)
                    @Html.HiddenFor(x => x.Fields[i].FieldValue)
                    @Html.HiddenFor(x => x.Fields[i].IsMandatory)
                </div>
                <div class="small-3 columns">
                    @Html.Raw(field.Description)
                </div>
            </div>
        }
        <div class="row m-b-20">
            <div class="small-6 columns">
                <a href="@Model.GdprLink" target="_blank">@Model.GdprLinkText</a>
            </div>
        </div>
        @if (!string.IsNullOrWhiteSpace(Model.TermsAndConditions) && !string.IsNullOrWhiteSpace(Model.TermsAndConditionsLabel))
        {
            <div class="row m-b-20">
                <div class="small-6 columns">
                    @Html.CheckBoxFor(x => x.AgreeTermsAndConditions)
                    <span>@Model.TermsAndConditionsLabel</span>
                </div>
            </div>
            <div class="row m-b-20">
                <div class="small-6 columns">
                    <span>@Html.Raw(Model.TermsAndConditions)</span>
                </div>
            </div>
        }
        <div class="row m-b-20">
            <div class="small-1 columns">
                <button class="button button--large" name="submitButton" value="NewTest" type="submit">@Model.ButtonText</button>
                @Html.HiddenFor(x => x.ButtonText)
                @Html.HiddenFor(x => x.Title)
                @Html.HiddenFor(x => x.EmailFrom)
                @Html.HiddenFor(x => x.EmailTo)
                @Html.HiddenFor(x => x.GdprLinkText)
                @Html.HiddenFor(x => x.GdprLink)
                @Html.HiddenFor(x => x.TermsAndConditions)
                @Html.HiddenFor(x => x.TermsAndConditionsLabel)
            </div>
        </div>
        @{
            var mandatoryErrors = ViewData.ModelState.Where(x => x.Key.Contains("-Mandatory")).Select(x => x.Value).SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            var invalidErrors = ViewData.ModelState.Where(x => x.Key.Contains("-Invalid")).Select(x => x.Value).SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
            var termsAndConditions = ViewData.ModelState.Where(x => x.Key.Contains("-TermsAndConditions")).Select(x => x.Value).SelectMany(x => x.Errors).Select(x => x.ErrorMessage).ToList();
        }
        @if (mandatoryErrors.Count > 0)
        {
            <div class="alert">
                <span>Mandatory Fields - @(String.Join(",", mandatoryErrors))</span>
            </div>
        }
        @if (invalidErrors.Count > 0)
        {
            <div class="alert">
                <span>Invalid Fields - @(String.Join(",", invalidErrors))</span>
            </div>
        }
        @if (termsAndConditions.Count > 0)
        {
            if (mandatoryErrors.Count > 0 || invalidErrors.Count > 0)
            {
                <br />
            }
            <div class="alert">
                <span> @(String.Join(",", termsAndConditions))</span>
            </div>
        }
        @if (ViewBag.SuccessMsg != null)
        {
            <div class="success">
                @ViewBag.SuccessMsg
            </div>
        }
    </div>
}